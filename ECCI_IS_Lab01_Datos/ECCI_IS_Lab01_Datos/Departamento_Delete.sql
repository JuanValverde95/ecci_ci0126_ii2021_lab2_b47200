﻿CREATE PROCEDURE [dbo].[Departamento_Delete] 
	@DepartamentoID INT 
AS 
BEGIN 
	DELETE FROM Departamento 
	WHERE DepartamentoID = @DepartamentoID 
END
