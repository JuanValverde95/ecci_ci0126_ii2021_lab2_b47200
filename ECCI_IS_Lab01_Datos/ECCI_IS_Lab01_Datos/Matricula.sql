﻿CREATE TABLE [dbo].[Matricula]
(
	[MatriculaId]		INT	IDENTITY (1,1) NOT NULL,
	[Nota]				DECIMAL(3,2) NOT NULL,
	[CursoId]			INT NOT NULL,
	[EstudianteId]		INT NOT NULL,
	PRIMARY KEY CLUSTERED ([MatriculaId] ASC),
	CONSTRAINT [fk_dbo.Matricula_dbo.Curso_CursoID] FOREIGN KEY ([CursoID])
		REFERENCES [dbo].[Curso] ([CursoID]) ON DELETE CASCADE,
	CONSTRAINT [fk_dbo.Matricula_dbo.Estudiante_EstudianteID] FOREIGN KEY ([EstudianteID])
		REFERENCES [dbo].[Estudiante] ([EstudianteID]) ON DELETE CASCADE
)
