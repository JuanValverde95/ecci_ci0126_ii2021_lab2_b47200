﻿CREATE TABLE [dbo].[Departamento]
(
	[DepartamentoID]	INT				IDENTITY (1,1) NOT NULL,
	[Nombre]			NVARCHAR (50)	NULL,
	[Presupuesto]		FLOAT			NULL
	PRIMARY KEY CLUSTERED ([DepartamentoID] ASC)
	
)
