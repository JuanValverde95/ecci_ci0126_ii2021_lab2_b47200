﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ECCI_IS_Lab01_WebApp.Models
{
    // Connection of EstudianteMetadata with Estudiante 
    [MetadataType(typeof(EstudianteMetadata))]
    public partial class Estudiante {}

    // Connection of MatriculaMetadata with Matricula
    [MetadataType(typeof(MatriculaMetadata))] 
    public partial class Matricula {}

    public class PartialClasses
    {

    }
}