﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ECCI_IS_Lab01_WebApp.Models
{
    public class EstudianteMetadata
    {
        [Display(Name = "Fecha")]
        public Nullable<System.DateTime> FechaMatricula { get; set; }
        [Display(Name = "Correo")]
        [DataType(DataType.EmailAddress)]
        [Required]
        public string CorreoElectronico { get; set; }
    }
}