﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ECCI_IS_Lab01_WebApp.Models
{
    public class MatriculaMetadata
    {
        [Range(4.0, 9.99)]
        public Nullable<decimal> Nota { get; set; }
    }
}